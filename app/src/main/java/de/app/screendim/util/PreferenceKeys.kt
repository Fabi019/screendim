package de.app.screendim.util

object PreferenceKeys {

    object General {

        const val KEY_AUTO_START_ON_BOOT = "pref_key_auto_start_on_boot"
        const val KEY_ENABLE_SCHEDULE = "pref_key_enable_schedule"
        const val KEY_AUTO_START_TIME_ON = "pref_key_auto_start_time_on"
        const val KEY_AUTO_START_TIME_ON_HOURS = "pref_key_auto_start_time_on_hours"
        const val KEY_AUTO_START_TIME_ON_MINUTES = "pref_key_auto_start_time_on_minutes"
        const val KEY_AUTO_START_TIME_OFF = "pref_key_auto_start_time_off"
        const val KEY_AUTO_START_TIME_OFF_HOURS = "pref_key_auto_start_time_off_hours"
        const val KEY_AUTO_START_TIME_OFF_MINUTES = "pref_key_auto_start_time_off_minutes"
        const val KEY_CUSTOM_SIZE_OFFSET = "pref_key_custom_size_offset"

        object Default {

            const val START_ON_BOOT = false
            const val ENABLE_SCHEDULE = false
            const val START_TIME_ON_HOURS = 22
            const val START_TIME_ON_MINUTES = 0
            const val START_TIME_OFF_HOURS = 8
            const val START_TIME_OFF_MINUTES = 0
            const val CUSTOM_SIZE_OFFSET = "100"
        }
    }

    object Shortcuts {

        const val KEY_OPEN_ACCSESIBILITY_SETTINGS = "pref_key_open_accessibility_settings"
        const val KEY_EXCLUDE_FROM_BATTERY_OPT = "pref_key_open_exclude_battery_optimization"
        const val KEY_MANAGE_NOTIFICATION = "pref_key_manage_notification"
    }

    object Appearance {

        const val KEY_LANGUAGE = "pref_key_appearance_language"
        const val KEY_THEME = "pref_key_appearance_theme"
        const val KEY_USE_BLACK_THEME = "pref_key_appearance_use_black_theme"

        object Theme {

            const val AUTO = "0"
            const val LIGHT = "1"
            const val DARK = "2"
        }

        object Default {

            const val LANGUAGE = "en"
            const val THEME = Theme.AUTO
            const val USE_BLACK_THEME = false
        }
    }

    object Overlay {

        /**
         * 0 -> Temperature
         * 1 -> RGB
         */
        const val KEY_USED_METHOD = "pref_key_overlay_method"
        const val KEY_LAST_COLOR = "pref_key_last_color"

        const val KEY_TEMPERATURE = "pref_key_overlay_temperature"
        const val KEY_INTENSITY = "pref_key_overlay_intensity"
        const val KEY_DIM = "pref_key_overlay_dim"

        const val KEY_RED = "pref_key_overlay_red"
        const val KEY_GREEN = "pref_key_overlay_green"
        const val KEY_BLUE = "pref_key_overlay_blue"
        const val KEY_ALPHA = "pref_key_overlay_alpha"

        object Default {

            const val METHOD = 0
            const val COLOR = 0x0
            const val TEMPERATURE = 5900f
            const val INTENSITY = 20f
            const val DIM = 50f
            const val RED = 100f
            const val GREEN = 100f
            const val BLUE = 100f
            const val ALPHA = 50f
        }

    }

    object About {
        const val KEY_VERSION_NAME = "pref_key_about_version_name"
        const val KEY_GITHUB_PAGE = "pref_key_about_github_repo"
    }

}