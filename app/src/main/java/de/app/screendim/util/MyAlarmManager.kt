package de.app.screendim.util

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import de.app.screendim.service.OverlayService
import java.util.*

object MyAlarmManager {
    private var calendar = Calendar.getInstance()

    private var pendingIntentOn: PendingIntent? = null
    private var pendingIntentOff: PendingIntent? = null

    fun setAlarmOn(context: Context, forceReschedule: Boolean = false) {
        calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, context.startTimeOnHours())
        calendar.set(Calendar.MINUTE, context.startTimeOnMinutes())
        calendar.set(Calendar.SECOND, 0)

        if (calendar.before(Calendar.getInstance()) || forceReschedule)
            calendar.add(Calendar.DATE, 1)

        pendingIntentOn = setAlarm(context, calendar.timeInMillis, "start_service_alarm")
    }

    fun setAlarmOff(context: Context, forceReschedule: Boolean = false) {
        calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, context.startTimeOffHours())
        calendar.set(Calendar.MINUTE, context.startTimeOffMinutes())
        calendar.set(Calendar.SECOND, 0)

        if (calendar.before(Calendar.getInstance()) || forceReschedule)
            calendar.add(Calendar.DATE, 1)

        pendingIntentOff = setAlarm(context, calendar.timeInMillis, "exit_service_alarm")
    }

    private fun setAlarm(context: Context, alarmTime: Long, action: String? = null): PendingIntent {
        val alarmManager: AlarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val intent = Intent(context, OverlayService::class.java)
            .setAction(action)

        var flags = PendingIntent.FLAG_UPDATE_CURRENT
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            flags = flags or PendingIntent.FLAG_IMMUTABLE

        val pendingIntent = PendingIntent.getService(context, 0, intent, flags)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                alarmTime,
                pendingIntent
            )
        else
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent)

        return pendingIntent
    }

    fun cancelAlarm(context: Context) {
        cancelAlarm(context, pendingIntentOn)
        cancelAlarm(context, pendingIntentOff)
        context.showToast("Schedule canceled!")
    }

    private fun cancelAlarm(context: Context, pendingIntent: PendingIntent?) {
        pendingIntent?.let {
            val alarmManager: AlarmManager =
                context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(it)
        }
    }

}