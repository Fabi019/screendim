package de.app.screendim.util

import android.app.Activity
import android.app.ActivityManager
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import de.app.screendim.MainActivity
import de.app.screendim.R
import de.app.screendim.SettingsActivity
import java.util.*
import kotlin.math.ln
import kotlin.math.pow

//// BEGIN Activity

fun Activity.restartApp() {
    val taskBuilder = TaskStackBuilder.create(this)
        .addNextIntent(Intent(this, MainActivity::class.java))
        .addNextIntent(Intent(this, SettingsActivity::class.java))
    finish()
    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    taskBuilder.startActivities()
}

//// END Activity


//// BEGIN Snackbar

fun showSnackbar(view: View?, msg: String, length: Int = Snackbar.LENGTH_SHORT) {
    newSnackbar(view, msg, length)?.show()
}

fun newSnackbar(view: View?, msg: String, length: Int = Snackbar.LENGTH_SHORT): Snackbar? {
    if (view != null) {
        return Snackbar.make(view, msg, length)
    }
    return null
}

//// END Snackbar

//// BEGIN Fragment

fun Fragment.inflateLayout(
    @LayoutRes layoutResId: Int, root: ViewGroup? = null,
    attachToRoot: Boolean = false
): View =
    activity!!.inflateLayout(layoutResId, root, attachToRoot)

//// END Fragment


//// BEGIN Context

// Bug find/workaround credit: https://github.com/drakeet/ToastCompat#why
fun Context.showToast(msg: CharSequence, length: Int = Toast.LENGTH_SHORT) {
    val toast = Toast.makeText(this, msg, length)
    if (Build.VERSION.SDK_INT <= 25) {
        try {
            val field = View::class.java.getDeclaredField("mContext")
            field.isAccessible = true
            field.set(toast.view, ToastViewContextWrapper(this))
        } catch (e: Exception) {
        }
    }
    toast.show()
}

private class ToastViewContextWrapper(base: Context) : ContextWrapper(base) {
    override fun getApplicationContext(): Context =
        ToastViewApplicationContextWrapper(baseContext.applicationContext)
}

private class ToastViewApplicationContextWrapper(base: Context) : ContextWrapper(base) {
    override fun getSystemService(name: String): Any {
        return if (name == Context.WINDOW_SERVICE) {
            ToastWindowManager(baseContext.getSystemService(name) as WindowManager)
        } else {
            super.getSystemService(name)
        }
    }
}

private class ToastWindowManager(val base: WindowManager) : WindowManager {
    override fun getDefaultDisplay(): Display = base.defaultDisplay

    override fun addView(view: View?, params: ViewGroup.LayoutParams?) {
        try {
            base.addView(view, params)
        } catch (e: WindowManager.BadTokenException) {
            Log.e("Toast", "caught BadTokenException crash")
        }
    }

    override fun updateViewLayout(view: View?, params: ViewGroup.LayoutParams?) =
        base.updateViewLayout(view, params)

    override fun removeView(view: View?) = base.removeView(view)

    override fun removeViewImmediate(view: View?) = base.removeViewImmediate(view)
}

fun Context.getAttributeDrawable(@AttrRes attrId: Int): Drawable? {
    val tv = TypedValue()
    theme.resolveAttribute(attrId, tv, true)
    return ContextCompat.getDrawable(this, tv.resourceId)
}

fun Context.getDefaultSharedPreferences(): SharedPreferences =
    PreferenceManager.getDefaultSharedPreferences(this)

private fun isDarkThemeTime() = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) !in 7..17

fun Context.isDarkThemeOn(): Boolean {
    val theme = getDefaultSharedPreferences()
        .getString(PreferenceKeys.Appearance.KEY_THEME, PreferenceKeys.Appearance.Default.THEME)
    return theme == PreferenceKeys.Appearance.Theme.DARK ||
            (theme == PreferenceKeys.Appearance.Theme.AUTO && isDarkThemeTime())
}

fun Context.isSuperDarkTheme(): Boolean {
    return getDefaultSharedPreferences().getBoolean(
        PreferenceKeys
            .Appearance.KEY_USE_BLACK_THEME, PreferenceKeys.Appearance.Default.USE_BLACK_THEME
    )
}

fun Context.getMethod(): Int = getDefaultSharedPreferences().getInt(
    PreferenceKeys.Overlay.KEY_USED_METHOD,
    PreferenceKeys.Overlay.Default.METHOD
)

fun Context.getTemperature(): Float = getDefaultSharedPreferences().getFloat(
    PreferenceKeys.Overlay.KEY_TEMPERATURE,
    PreferenceKeys.Overlay.Default.TEMPERATURE
)

fun Context.getIntensity(): Float = getDefaultSharedPreferences().getFloat(
    PreferenceKeys.Overlay.KEY_INTENSITY,
    PreferenceKeys.Overlay.Default.INTENSITY
)

fun Context.getDim(): Float = getDefaultSharedPreferences().getFloat(
    PreferenceKeys.Overlay.KEY_DIM,
    PreferenceKeys.Overlay.Default.DIM
)

fun Context.getRed(): Float = getDefaultSharedPreferences().getFloat(
    PreferenceKeys.Overlay.KEY_RED,
    PreferenceKeys.Overlay.Default.RED
)

fun Context.getGreen(): Float = getDefaultSharedPreferences().getFloat(
    PreferenceKeys.Overlay.KEY_GREEN,
    PreferenceKeys.Overlay.Default.GREEN
)

fun Context.getBlue(): Float = getDefaultSharedPreferences().getFloat(
    PreferenceKeys.Overlay.KEY_BLUE,
    PreferenceKeys.Overlay.Default.BLUE
)

fun Context.getAlpha(): Float = getDefaultSharedPreferences().getFloat(
    PreferenceKeys.Overlay.KEY_ALPHA,
    PreferenceKeys.Overlay.Default.ALPHA
)

fun Context.getLastColor(): Int = getDefaultSharedPreferences().getInt(
    PreferenceKeys.Overlay.KEY_LAST_COLOR,
    PreferenceKeys.Overlay.Default.COLOR
)

fun Context.startOnBoot(): Boolean = getDefaultSharedPreferences().getBoolean(
    PreferenceKeys.General.KEY_AUTO_START_ON_BOOT,
    PreferenceKeys.General.Default.START_ON_BOOT
)

fun Context.scheduleEnabled(): Boolean = getDefaultSharedPreferences().getBoolean(
    PreferenceKeys.General.KEY_ENABLE_SCHEDULE,
    PreferenceKeys.General.Default.ENABLE_SCHEDULE
)

fun Context.startTimeOnHours(): Int = getDefaultSharedPreferences().getInt(
    PreferenceKeys.General.KEY_AUTO_START_TIME_ON_HOURS,
    PreferenceKeys.General.Default.START_TIME_ON_HOURS
)

fun Context.startTimeOnMinutes(): Int = getDefaultSharedPreferences().getInt(
    PreferenceKeys.General.KEY_AUTO_START_TIME_ON_MINUTES,
    PreferenceKeys.General.Default.START_TIME_ON_MINUTES
)

fun Context.startTimeOffHours(): Int = getDefaultSharedPreferences().getInt(
    PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_HOURS,
    PreferenceKeys.General.Default.START_TIME_OFF_HOURS
)

fun Context.startTimeOffMinutes(): Int = getDefaultSharedPreferences().getInt(
    PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_MINUTES,
    PreferenceKeys.General.Default.START_TIME_OFF_MINUTES
)

fun Context.getCustomOffset(): Int = Integer.valueOf(
    getDefaultSharedPreferences().getString(
        PreferenceKeys.General.KEY_CUSTOM_SIZE_OFFSET,
        PreferenceKeys.General.Default.CUSTOM_SIZE_OFFSET
    )!!
)

fun Context.getLanguage(): String = getDefaultSharedPreferences().getString(
    PreferenceKeys.Appearance.KEY_LANGUAGE,
    PreferenceKeys.Appearance.Default.LANGUAGE
)!!

private fun Context.setThemeAuto() {
    if (isDarkThemeTime()) {
        setThemeDark()
    } else {
        setThemeLight()
    }
}

private fun Context.setThemeDark() {
    val useBlackTheme = getDefaultSharedPreferences().getBoolean(
        PreferenceKeys
            .Appearance.KEY_USE_BLACK_THEME, PreferenceKeys.Appearance.Default.USE_BLACK_THEME
    )
    if (useBlackTheme) {
        setTheme(R.style.BlackTheme)
    } else {
        setTheme(R.style.DarkTheme)
    }
}

private fun Context.setThemeLight() {
    setTheme(R.style.LightTheme)
}

fun Context.setTheme() {
    val prefs = getDefaultSharedPreferences()
    val theme = prefs.getString(
        PreferenceKeys.Appearance.KEY_THEME,
        PreferenceKeys.Appearance.Default.THEME
    )
    when (theme) {
        PreferenceKeys.Appearance.Theme.AUTO -> setThemeAuto()
        PreferenceKeys.Appearance.Theme.DARK -> setThemeDark()
        PreferenceKeys.Appearance.Theme.LIGHT -> setThemeLight()
    }
}

fun Context.setLocale(locale: Locale) {
    Locale.setDefault(locale)
    val res = resources
    val dm = res.displayMetrics
    val conf = res.configuration
    conf.setLocale(locale)
    res.updateConfiguration(conf, dm)
}

fun <T> Context.isServiceRunning(service: Class<T>) =
    (getSystemService(ACTIVITY_SERVICE) as ActivityManager)
        .getRunningServices(Integer.MAX_VALUE)
        .any { it.service.className == service.name }

fun Context.inflateLayout(
    @LayoutRes layoutResId: Int, root: ViewGroup? = null,
    attachToRoot: Boolean = false
): View =
    LayoutInflater.from(this).inflate(layoutResId, root, attachToRoot)

//// END Context

fun tempToRGB(kelvin: Int): Array<Int> {
    val temp = kelvin / 200.0
    var (r, g, b) = arrayOf(0.0, 0.0, 0.0)
    if (temp < 66) {
        r = 255.0
        g = temp
        g = 99.4708025861 * ln(g) - 161.1195681661
        if (temp <= 19) {
            b = 0.0
        } else {
            b = temp - 10
            b = 138.5177312231 * ln(b) - 305.0447927307
        }
    } else {
        r = temp - 60
        r = 329.698727446 * r.pow(-0.1332047592)
        g = temp - 60
        g = 288.1221695283 * g.pow(-0.0755148492)
        b = 255.0
    }
    return arrayOf(
        r.toInt().coerceIn(0, 255),
        g.toInt().coerceIn(0, 255),
        b.toInt().coerceIn(0, 255)
    )
}

fun packARGB(alpha: Int, red: Int, green: Int, blue: Int): Int {
    return alpha shl 24 or
            (red shl 16) or
            (green shl 8) or
            blue
}

fun setLightPercentage(red: Int, green: Int, blue: Int, alpha: Int, percent: Int): Int {
    val r = (red * (100 + percent) / 100f).toInt()
    val g = (green * (100 + percent) / 100f).toInt()
    val b = (blue * (100 + percent) / 100f).toInt()
    return packARGB(
        alpha,
        if (r < 255) r else 255,
        if (b < 255) b else 255,
        if (g < 255) g else 255
    )
}