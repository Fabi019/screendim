package de.app.screendim

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.view.menu.MenuItemImpl
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import androidx.viewpager2.widget.ViewPager2
import com.adriangl.overlayhelper.OverlayHelper
import com.adriangl.overlayhelper.OverlayHelper.OverlayPermissionChangedListener
import com.google.android.material.tabs.TabLayout
import de.app.screendim.fragments.OverlaySettingFragments
import de.app.screendim.fragments.ViewStateAdapter
import de.app.screendim.service.OverlayService
import de.app.screendim.util.getMethod
import de.app.screendim.util.isServiceRunning
import de.app.screendim.util.showToast


class MainActivity : BaseActivity() {

    companion object {
        private const val EXIT_DOUBLE_PRESS_DELAY: Long = 2000
    }

    private var canExit = false

    private val exitRunnable = Runnable {
        canExit = false
    }

    private val tabLayout: TabLayout by lazy { findViewById(R.id.typeSelector) }
    private val viewPager: ViewPager2 by lazy { findViewById(R.id.pager) }
    private lateinit var menuPlayPause: MenuItemImpl

    private lateinit var overlayHelper: OverlayHelper

    private var overlayService: OverlayService? = null
    private var bound = false
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Log.d("MainActivity", "onServiceConnected")

            val binder = service as OverlayService.LocalBinder
            overlayService = binder.getService()
            val fragment = supportFragmentManager.findFragmentByTag("f" + viewPager.currentItem)
                    as OverlaySettingFragments.ImageModifyingFragment?
            if (fragment != null)
                overlayService?.updateColor(fragment.getCurrentColor())

            binder.exitRunnable = {
                stopOverlay()
            }

            bound = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d("MainActivity", "onServiceDisconnected")
            overlayService = null
        }
    }

    private val mPlayToPauseAnim: AnimatedVectorDrawableCompat by lazy {
        AnimatedVectorDrawableCompat.create(
            this,
            R.drawable.play_to_pause
        )!!
    }
    private val mPauseToPlayAnim: AnimatedVectorDrawableCompat by lazy {
        AnimatedVectorDrawableCompat.create(
            this,
            R.drawable.pause_to_play
        )!!
    }

    private var running = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupToolbar()

        val viewStateAdapter = ViewStateAdapter(supportFragmentManager, lifecycle)
        viewPager.adapter = viewStateAdapter

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

        viewPager.setPageTransformer { view, _ ->
            view.post {
                val wMeasureSpec =
                    View.MeasureSpec.makeMeasureSpec(view.width, View.MeasureSpec.EXACTLY)
                val hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                view.measure(wMeasureSpec, hMeasureSpec)
                viewPager.layoutParams =
                    (viewPager.layoutParams).also { lp -> lp.height = view.measuredHeight }
                viewPager.invalidate()
            }
        }

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                tabLayout.selectTab(tabLayout.getTabAt(position))
                val fragment = supportFragmentManager.findFragmentByTag("f$position")
                        as OverlaySettingFragments.ImageModifyingFragment?
                fragment?.updateImage()
            }
        })

        //viewPager.offscreenPageLimit = 2
        viewPager.currentItem = getMethod()

        overlayHelper =
            OverlayHelper(applicationContext, object : OverlayPermissionChangedListener {
                override fun onOverlayPermissionCancelled() {
                    showToast("Draw overlay permissions request canceled")
                }

                override fun onOverlayPermissionGranted() {
                    showToast("Draw overlay permissions request granted")
                }

                override fun onOverlayPermissionDenied() {
                    showToast("Draw overlay permissions request denied")
                }
            })

        overlayHelper.startWatching()

        if (!overlayHelper.canDrawOverlays()) {
            overlayHelper.requestDrawOverlaysPermission(
                this,
                getString(R.string.overlay_permission_title),
                getString(R.string.overlay_permission_message),
                getString(android.R.string.ok),
                getString(android.R.string.cancel)
            )
        }
    }

    override fun getToolbarIdRes(): Int = R.id.toolbar

    override fun getToolbarTitle(): String = getString(R.string.app_name)

    override fun onBackPressed() {
        if (canExit) {
            handler.removeCallbacks(exitRunnable)
            super.onBackPressed()
        } else {
            canExit = true
            showToast(getString(R.string.press_back_again_to_exit))
            handler.postDelayed(exitRunnable, EXIT_DOUBLE_PRESS_DELAY)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        overlayService?.let {
            if (bound)
                unbindService(serviceConnection)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        menuPlayPause = menu.findItem(R.id.action_apply) as MenuItemImpl
        if (isServiceRunning(OverlayService::class.java))
            startOverlay()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_apply -> {
            if (!running && (overlayService == null) && overlayHelper.canDrawOverlays()) {
                startOverlay()
            } else if (running) {
                stopOverlay()
            } else {
                showToast("Permission not granted!")
            }
            true
        }
        R.id.action_save -> {
            val fragment = supportFragmentManager.findFragmentByTag("f" + viewPager.currentItem)
                    as OverlaySettingFragments.ImageModifyingFragment
            fragment.saveValues()
            if (running) {
                overlayService?.updateColor(fragment.getCurrentColor())
            }
            true
        }
        R.id.action_settings -> {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        }
        R.id.action_exit -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun startOverlay() {
        bindService(
            Intent(this@MainActivity, OverlayService::class.java),
            serviceConnection,
            BIND_AUTO_CREATE
        )
        startService(Intent(this@MainActivity, OverlayService::class.java))
        updatePlayPauseButton()
    }

    private fun stopOverlay() {
        if (bound)
            unbindService(serviceConnection)
        bound = false
        stopService(Intent(this@MainActivity, OverlayService::class.java))
        overlayService = null
        updatePlayPauseButton()
    }

    @SuppressLint("RestrictedApi")
    private fun updatePlayPauseButton() {
        var anim = mPlayToPauseAnim
        if (running)
            anim = mPauseToPlayAnim
        menuPlayPause.icon = anim
        anim.start()
        running = !running
    }

}