package de.app.screendim.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import de.app.screendim.util.*
import java.util.*

class BroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (Intent.ACTION_BOOT_COMPLETED == intent?.action) {
            if (context != null) {
                if (context.startOnBoot()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        context.startForegroundService(Intent(context, OverlayService::class.java))
                    else
                        context.startService(Intent(context, OverlayService::class.java))
                }
                if (context.scheduleEnabled()) {
                    MyAlarmManager.setAlarmOn(context)
                    MyAlarmManager.setAlarmOff(context)
                }
            }

        }
    }

}