package de.app.screendim.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PixelFormat
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import androidx.core.app.NotificationCompat
import de.app.screendim.MainActivity
import de.app.screendim.OverlayView
import de.app.screendim.R
import de.app.screendim.util.MyAlarmManager
import de.app.screendim.util.getCustomOffset
import de.app.screendim.util.getLastColor
import kotlin.math.max


class OverlayService : Service() {

    companion object {
        const val ONGOING_NOTIFICATION_ID = 6661
        const val CHANNEL_ID = "overlay_channel_id"
        const val CHANNEL_NAME = "Overlay Service"
    }

    private lateinit var rootView: View
    private lateinit var overlaySurfaceView: OverlayView
    private lateinit var windowManager: WindowManager

    private val binder = LocalBinder()
    override fun onBind(p0: Intent?): IBinder = binder

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.action != null) {
            if (intent.action!!.startsWith("exit_service")) {
                if (intent.action.equals("exit_service_alarm")) {
                    // force a reschedule
                    MyAlarmManager.setAlarmOff(this, true)
                }
                runCatching {
                    binder.exitRunnable.invoke()
                }
                stopSelf()
                return START_STICKY
            } else if (intent.action.equals("start_service_alarm")) {
                // force a reschedule
                MyAlarmManager.setAlarmOn(this, true)
            }
        }
        startForeground()
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()

        initOverlay()
    }

    override fun onDestroy() {
        super.onDestroy()

        windowManager.removeView(rootView)
    }

    private fun startForeground() {
        var intentFlag = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            intentFlag = PendingIntent.FLAG_IMMUTABLE

        val pendingIntent: PendingIntent =
            Intent(this, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            }.let { ni ->
                PendingIntent.getActivity(this, 0, ni, intentFlag)
            }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE)
            channel.lightColor = Color.BLUE
            channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nm.createNotificationChannel(channel)
        }

        val pendingExitIntent: PendingIntent =
            Intent(this, OverlayService::class.java).apply {
                action = "exit_service"
            }.let { ni ->
                PendingIntent.getService(this, 0, ni, intentFlag)
            }

        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            //.setContentTitle(getText(R.string.app_name))
            .setContentText("Touch to open")
            .setSmallIcon(R.drawable.ic_notification)
            .setContentIntent(pendingIntent)
            .setTicker(getText(R.string.app_name))
            .addAction(android.R.drawable.ic_delete, "Exit", pendingExitIntent)
            .build()

        startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    private fun initOverlay() {
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val li = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        rootView = li.inflate(R.layout.overlay_view, null)
        overlaySurfaceView = rootView.findViewById(R.id.overlaySurfaceView)

        val type = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY
        else
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY

        val (width, height) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val bounds = windowManager.currentWindowMetrics.bounds
            bounds.width() to bounds.height()
        } else {
            val display = windowManager.defaultDisplay
            val metrics = DisplayMetrics()
            display?.getRealMetrics(metrics)
            metrics.widthPixels to metrics.heightPixels
        }

        val maxSize = max(width, height)
        val offset = applicationContext.getCustomOffset()

        val params = WindowManager.LayoutParams(
            maxSize + offset * 2, maxSize + offset * 2, -offset, -offset,
            type,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                    or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    or WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS
                    or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
                    or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
                    or WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
            PixelFormat.TRANSLUCENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            params.alpha = 0.8f

        windowManager.addView(rootView, params)
        updateColor(applicationContext.getLastColor())
    }

    fun updateColor(color: Int) {
        overlaySurfaceView.updateColor(color)
    }

    inner class LocalBinder : Binder() {
        lateinit var exitRunnable: () -> Unit
        fun getService(): OverlayService = this@OverlayService
    }
}