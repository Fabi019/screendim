package de.app.screendim.service

import android.content.Intent
import android.os.Build
import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import androidx.annotation.RequiresApi
import de.app.screendim.util.isServiceRunning

@RequiresApi(Build.VERSION_CODES.N)
class MyTileService : TileService() {
    override fun onTileAdded() {
        super.onTileAdded()
        updateTileState()
    }

    override fun onStartListening() {
        super.onStartListening()
        updateTileState()
    }

    override fun onClick() {
        super.onClick()

        qsTile.state = when (qsTile.state) {
            Tile.STATE_INACTIVE -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    startForegroundService(Intent(this, OverlayService::class.java))
                else
                    startService(Intent(this, OverlayService::class.java))
                Tile.STATE_ACTIVE
            }
            Tile.STATE_ACTIVE -> {
                stopService(Intent(this, OverlayService::class.java))
                Tile.STATE_INACTIVE
            }
            else -> qsTile.state
        }

        qsTile.updateTile()
    }

    private fun updateTileState() {
        qsTile.state = when (isServiceRunning(OverlayService::class.java)) {
            true -> Tile.STATE_ACTIVE
            else -> Tile.STATE_INACTIVE
        }
        qsTile.updateTile()
    }
}