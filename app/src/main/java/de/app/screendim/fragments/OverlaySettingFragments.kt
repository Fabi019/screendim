package de.app.screendim.fragments

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.google.android.material.slider.Slider
import de.app.screendim.R
import de.app.screendim.util.*

class OverlaySettingFragments {

    abstract class ImageModifyingFragment : Fragment() {
        private val image: ImageView by lazy { requireActivity().findViewById(R.id.imageView) }
        abstract fun saveValues()
        abstract fun getCurrentColor(): Int
        fun updateImage() {
            image.setColorFilter(getCurrentColor(), PorterDuff.Mode.SRC_OVER)
        }
    }

    class RGBSetting : ImageModifyingFragment() {
        private val redSlider: Slider by lazy { requireView().findViewById(R.id.redBar) }
        private val greenSlider: Slider by lazy { requireView().findViewById(R.id.greenBar) }
        private val blueSlider: Slider by lazy { requireView().findViewById(R.id.blueBar) }
        private val alphaSlider: Slider by lazy { requireView().findViewById(R.id.alphaBar) }

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            return inflater.inflate(R.layout.rgb_setting_fragment, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            with(requireContext()) {
                redSlider.value = getRed()
                greenSlider.value = getGreen()
                blueSlider.value = getBlue()
                alphaSlider.value = getAlpha()
            }

            val changeListener =
                Slider.OnChangeListener { _, _, _ ->
                    updateImage()
                }

            redSlider.addOnChangeListener(changeListener)
            greenSlider.addOnChangeListener(changeListener)
            blueSlider.addOnChangeListener(changeListener)
            alphaSlider.addOnChangeListener(changeListener)

            updateImage()
        }

        override fun saveValues() {
            with(requireContext()) {
                getDefaultSharedPreferences().edit()
                    .putInt(PreferenceKeys.Overlay.KEY_USED_METHOD, 1)
                    .putFloat(PreferenceKeys.Overlay.KEY_RED, redSlider.value)
                    .putFloat(PreferenceKeys.Overlay.KEY_GREEN, greenSlider.value)
                    .putFloat(PreferenceKeys.Overlay.KEY_BLUE, blueSlider.value)
                    .putFloat(PreferenceKeys.Overlay.KEY_ALPHA, alphaSlider.value)
                    .putInt(PreferenceKeys.Overlay.KEY_LAST_COLOR, getCurrentColor())
                    .apply()
                showToast("RGB values saved!")
            }
        }

        override fun getCurrentColor(): Int = packARGB(
            alphaSlider.value.toInt(),
            redSlider.value.toInt(),
            greenSlider.value.toInt(),
            blueSlider.value.toInt()
        )
    }

    class TempSetting : ImageModifyingFragment() {
        private val tempSlider: Slider by lazy { requireView().findViewById(R.id.temperatureBar) }
        private val intensitySlider: Slider by lazy { requireView().findViewById(R.id.intensityBar) }
        private val dimSlider: Slider by lazy { requireView().findViewById(R.id.dimBar) }

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            return inflater.inflate(R.layout.temp_setting_fragment, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            with(requireContext()) {
                tempSlider.value = getTemperature()
                intensitySlider.value = getIntensity()
                dimSlider.value = getDim()
            }

            val changeListener =
                Slider.OnChangeListener { _, _, _ ->
                    updateImage()
                }

            tempSlider.addOnChangeListener(changeListener)
            intensitySlider.addOnChangeListener(changeListener)
            dimSlider.addOnChangeListener(changeListener)

            updateImage()
        }

        override fun saveValues() {
            with(requireContext()) {
                getDefaultSharedPreferences().edit()
                    .putInt(PreferenceKeys.Overlay.KEY_USED_METHOD, 0)
                    .putFloat(PreferenceKeys.Overlay.KEY_TEMPERATURE, tempSlider.value)
                    .putFloat(PreferenceKeys.Overlay.KEY_INTENSITY, intensitySlider.value)
                    .putFloat(PreferenceKeys.Overlay.KEY_DIM, dimSlider.value)
                    .putInt(PreferenceKeys.Overlay.KEY_LAST_COLOR, getCurrentColor())
                    .apply()
                showToast("Temp. values saved!")
            }
        }

        override fun getCurrentColor(): Int {
            var (r, g, b) = tempToRGB(tempSlider.value.toInt())
            r = (r * (intensitySlider.value / 100)).toInt()
            g = (g * (intensitySlider.value / 100)).toInt()
            b = (b * (intensitySlider.value / 100)).toInt()
            return setLightPercentage(
                r,
                g,
                b,
                ((dimSlider.value / 200) * 255).toInt() + 100,
                -dimSlider.value.toInt()
            )
        }
    }

}