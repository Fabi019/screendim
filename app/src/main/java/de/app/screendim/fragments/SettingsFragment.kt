package de.app.screendim.fragments

import android.annotation.SuppressLint
import android.content.Context.POWER_SERVICE
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.text.InputType
import androidx.preference.*
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import de.app.screendim.R
import de.app.screendim.util.*


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings)
        setupGeneralCategory()
        setupShortcutsCategory()
        setupAppearanceCategory()
        setupAboutCategory()
    }

    private fun setupGeneralCategory() {
        val sharedPrefs = preferenceScreen.sharedPreferences!!

        val customOffset =
            findPreference<EditTextPreference>(PreferenceKeys.General.KEY_CUSTOM_SIZE_OFFSET)!!
        customOffset.setOnBindEditTextListener {
            it.inputType = InputType.TYPE_CLASS_NUMBER
        }

        val enableSchedule =
            findPreference<SwitchPreferenceCompat>(PreferenceKeys.General.KEY_ENABLE_SCHEDULE)!!
        val scheduleTimeOn =
            findPreference<Preference>(PreferenceKeys.General.KEY_AUTO_START_TIME_ON)!!
        val scheduleTimeOff =
            findPreference<Preference>(PreferenceKeys.General.KEY_AUTO_START_TIME_OFF)!!

        val startTimeHour = sharedPrefs.getInt(
            PreferenceKeys.General.KEY_AUTO_START_TIME_ON_HOURS,
            PreferenceKeys.General.Default.START_TIME_ON_HOURS
        )
        val startTimeMinute = sharedPrefs.getInt(
            PreferenceKeys.General.KEY_AUTO_START_TIME_ON_MINUTES,
            PreferenceKeys.General.Default.START_TIME_ON_MINUTES
        )
        val stopTimeHour = sharedPrefs.getInt(
            PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_HOURS,
            PreferenceKeys.General.Default.START_TIME_OFF_HOURS
        )
        val stopTimeMinutes = sharedPrefs.getInt(
            PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_MINUTES,
            PreferenceKeys.General.Default.START_TIME_OFF_MINUTES
        )

        scheduleTimeOn.summary = "%02d:%02d".format(startTimeHour, startTimeMinute)
        scheduleTimeOff.summary = "%02d:%02d".format(stopTimeHour, stopTimeMinutes)

        scheduleTimeOn.isEnabled = enableSchedule.isChecked
        scheduleTimeOff.isEnabled = enableSchedule.isChecked

        enableSchedule.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { _, newValue ->
                val state = newValue as Boolean
                scheduleTimeOn.isEnabled = state
                scheduleTimeOff.isEnabled = state
                if (state) {
                    MyAlarmManager.setAlarmOn(requireContext())
                    MyAlarmManager.setAlarmOff(requireContext())
                } else
                    MyAlarmManager.cancelAlarm(requireContext())
                true
            }

        scheduleTimeOn.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            val hour = sharedPrefs.getInt(
                PreferenceKeys.General.KEY_AUTO_START_TIME_ON_HOURS,
                PreferenceKeys.General.Default.START_TIME_ON_HOURS
            )
            val minute = sharedPrefs.getInt(
                PreferenceKeys.General.KEY_AUTO_START_TIME_ON_MINUTES,
                PreferenceKeys.General.Default.START_TIME_ON_MINUTES
            )
            val picker = MaterialTimePicker.Builder()
                .setHour(hour)
                .setMinute(minute)
                .setTimeFormat(TimeFormat.CLOCK_24H)
                .build()
            picker.addOnPositiveButtonClickListener {
                sharedPrefs.edit()
                    .putInt(PreferenceKeys.General.KEY_AUTO_START_TIME_ON_HOURS, picker.hour)
                    .putInt(PreferenceKeys.General.KEY_AUTO_START_TIME_ON_MINUTES, picker.minute)
                    .apply()
                scheduleTimeOn.summary = "%02d:%02d".format(picker.hour, picker.minute)
                MyAlarmManager.setAlarmOn(requireContext())
            }
            picker.show(requireActivity().supportFragmentManager, "TimePickerOn")
            false
        }

        scheduleTimeOff.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            val hour = sharedPrefs.getInt(
                PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_HOURS,
                PreferenceKeys.General.Default.START_TIME_OFF_HOURS
            )
            val minute = sharedPrefs.getInt(
                PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_MINUTES,
                PreferenceKeys.General.Default.START_TIME_OFF_MINUTES
            )
            val picker = MaterialTimePicker.Builder()
                .setHour(hour)
                .setMinute(minute)
                .setTimeFormat(TimeFormat.CLOCK_24H)
                .build()
            picker.addOnPositiveButtonClickListener {
                sharedPrefs.edit()
                    .putInt(PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_HOURS, picker.hour)
                    .putInt(PreferenceKeys.General.KEY_AUTO_START_TIME_OFF_MINUTES, picker.minute)
                    .apply()
                scheduleTimeOff.summary = "%02d:%02d".format(picker.hour, picker.minute)
                MyAlarmManager.setAlarmOff(requireContext())
            }
            picker.show(requireActivity().supportFragmentManager, "TimePickerOff")
            false
        }
    }

    @SuppressLint("BatteryLife")
    private fun setupShortcutsCategory() {
        val openAccessibilitySettings =
            findPreference<Preference>(PreferenceKeys.Shortcuts.KEY_OPEN_ACCSESIBILITY_SETTINGS)!!
        openAccessibilitySettings.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            true
        }

        val excludeFromBatteryOpt =
            findPreference<Preference>(PreferenceKeys.Shortcuts.KEY_EXCLUDE_FROM_BATTERY_OPT)!!
        excludeFromBatteryOpt.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val packageName: String = requireContext().packageName
                val pm = requireContext().getSystemService(POWER_SERVICE) as PowerManager?
                if (pm?.isIgnoringBatteryOptimizations(packageName) == false) {
                    val intent = Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
                    intent.data = Uri.parse("package:$packageName")
                    startActivity(intent)
                } else {
                    val intent = Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS)
                    startActivity(intent)
                }
            } else {
                requireContext().showToast("Only available for Android > 6.0")
            }
            true
        }

        val manageNotification =
            findPreference<Preference>(PreferenceKeys.Shortcuts.KEY_MANAGE_NOTIFICATION)!!
        manageNotification.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val settingsIntent: Intent = Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(Settings.EXTRA_APP_PACKAGE, requireContext().packageName)
                startActivity(settingsIntent)
            } else {
                requireContext().showToast("Only available for Android > 8.0")
            }
            true
        }
    }

    private fun setupAppearanceCategory() {
        val sharedPrefs = preferenceScreen.sharedPreferences!!

        val themePref = findPreference<ListPreference>(PreferenceKeys.Appearance.KEY_THEME)!!
        val useBlackThemePref =
            findPreference<Preference>(PreferenceKeys.Appearance.KEY_USE_BLACK_THEME)!!
        val langPref = findPreference<ListPreference>(PreferenceKeys.Appearance.KEY_LANGUAGE)!!

        val currTheme = sharedPrefs.getString(
            PreferenceKeys.Appearance.KEY_THEME,
            PreferenceKeys.Appearance.Default.THEME
        )!!

        when (currTheme) {
            PreferenceKeys.Appearance.Theme.AUTO,
            PreferenceKeys.Appearance.Theme.DARK -> useBlackThemePref.isEnabled = true
            else -> useBlackThemePref.isEnabled = false
        }

        val themePrefEntries = resources.getStringArray(R.array.pref_appearance_theme_entries)
        themePref.summary = themePrefEntries[currTheme.toInt()]

        themePref.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { preference, newValue ->
                preference.summary = themePrefEntries[(newValue as String).toInt()]
                requireActivity().restartApp()
                true
            }

        useBlackThemePref.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { _, _ ->
                if (requireActivity().isDarkThemeOn()) {
                    requireActivity().restartApp()
                }
                true
            }

        val langPrefEntries = resources.getStringArray(R.array.pref_appearance_language_entries)
        langPref.summary = langPrefEntries[langPref.findIndexOfValue(langPref.value)]

        langPref.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { preference, newValue ->
                preference.summary =
                    langPrefEntries[(preference as ListPreference).findIndexOfValue(
                        newValue as String?
                    )]
                requireActivity().restartApp()
                true
            }
    }

    private var counter: Int = 0

    private fun setupAboutCategory() {
        val prefGit = findPreference<Preference>(PreferenceKeys.About.KEY_GITHUB_PAGE)!!
        prefGit.summary = "https://gitlab.com/Fabi019/screendim"
        prefGit.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            val intent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/Fabi019/screendim"))
            startActivity(intent)
            true
        }

        val prefAbout = findPreference<Preference>(PreferenceKeys.About.KEY_VERSION_NAME)!!
        prefAbout.summary = "Version ${
            with(requireActivity()) {
                packageManager.getPackageInfo(packageName, 0)
            }.versionName
        }"

        prefAbout.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            if (++counter > 6) requireContext().showToast("¯\\_(ツ)_/¯").also { counter = 0 }
            true
        }
    }
}
