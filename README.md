# ScreenDim

Simple Android app that creates a adjustable transparent overlay. This can be useful for example to
dim the screen further down than the brightness slider of the smartphone allows and/or act like a
bluelight filter. Requires at least an device with Android 5.0 (API level 21).

Get the APK file from the latest build
pipeline ([Direct download](https://gitlab.com/Fabi019/screendim/-/jobs/artifacts/master/raw/app/build/outputs/apk/debug/app-debug.apk?job=assembleDebug))

## Features

* Two ways for adjusting the overlay color (RGB and Temperature)
* Preview image above the sliders to see how the overlay would look like
* Option to start the filter on device boot
* Integrated scheduling for enabling and disabling the overlay at specific times
* Controllable via quick settings
* Light/Dark and super dark theme

## Screenshots

![Preview](preview/Preview.png)
